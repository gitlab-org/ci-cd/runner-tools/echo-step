// Package main provides a binary that echos a string verbatim, with
// an option to reverse the string by bytes.  We build and distribute
// the binary in the repository under /bin because steps does not yet
// have a way to distribute binaries that doesn't rely on Docker.
package main

import (
	"flag"
	"fmt"
	"os"
	"slices"
)

var (
	outputFile   = flag.String("output-file", "", "File in which to output echo in k:v format")
	echo         = flag.String("echo", "", "String to echo verbatim")
	reverseBytes = flag.Bool("reverse-bytes", false, "Reverse echo as byte slice before returning")
)

func main() {
	flag.Parse()
	out := []byte(*echo)
	if *reverseBytes {
		slices.Reverse(out)
	}
	fmt.Println(string(out))
	err := os.WriteFile(*outputFile, []byte(fmt.Sprintf(`{"name":"echo", "value":%q}`, string(out))), 0660)
	if err != nil {
		panic(err)
	}
}
