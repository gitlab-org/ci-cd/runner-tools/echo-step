.PHONY: build

build:
	GOOS="$(OS_TYPE)" GOARCH="$(ARCH)" go build -o bin/echo .

build_linux_amd64: OS_TYPE=linux
build_linux_amd64: ARCH=amd64
build_linux_amd64: build
